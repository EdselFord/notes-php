<?php
session_start();

if (!isset($_SESSION['user'])) {
    header("Location: auth.php");
}

include "database/user.php";
include "database/notes.php";
include "database/collab.php";

$user = User::getById($_SESSION['user']);
$notes = Notes::read($_SESSION['user']);

$notes = array_merge($notes, Collab::getNotesFromCollab($_SESSION['user']));

?>

<form action="process/auth.php" method="post">
    Logged by <?= $user['username'] ?> <button type="submit" name="logout">Log Out</button>
</form>


<h1>Notes Apps</h1>
<form action="process/notes.php" method="post">
    <input name="name" type="text" autocomplete="off" required>
    <button name="create" type="submit">Create new Notes</button>
</form>

<?php if (empty($notes)) : ?>
    Please try add some note(s)
<?php die();
endif; ?>

<div style="display: grid; grid-template-columns: repeat(3, 1fr); grid-gap: 20px;">
    <?php foreach ($notes as $note) : ?>
        <fieldset style="height: 160px;" onclick="document.location.href = './display.php?id=<?= $note['id'] ?>'">
            <h3 style="margin: 0"><?= htmlspecialchars($note["name"])  ?></h3>
            <h6 style="font-weight: normal; margin: 0">Updated at <?= date("D F j Y", strtotime($note['updateAt'])) ?></h6>
            <p style="max-height: 70px; overflow: hidden;"><?= htmlspecialchars($note['description'])  ?></p>
            <div style="display: flex; flex-direction: row;">
                <?php if ($note['tags'] != "") : foreach (explode(",", $note['tags']) as $tag) : ?>
                        <div style="width: fit-content; height: min-content; padding: 2px; margin: 1px; border: 1px solid black;">
                            <p style="font-size: xx-small; margin:  0"><?= $tag ?></p>
                        </div>
                <?php endforeach;
                endif ?>
            </div>
        </fieldset>
    <?php endforeach ?>
</div>