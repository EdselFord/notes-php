-- phpMyAdmin SQL Dump
-- version 5.2.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Aug 04, 2023 at 04:37 AM
-- Server version: 10.4.28-MariaDB
-- PHP Version: 8.0.28

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `notes`
--

-- --------------------------------------------------------

--
-- Table structure for table `collaboration`
--

CREATE TABLE `collaboration` (
  `id` int(11) NOT NULL,
  `notes_id` int(11) NOT NULL,
  `collaborator_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Dumping data for table `collaboration`
--

INSERT INTO `collaboration` (`id`, `notes_id`, `collaborator_id`) VALUES
(7, 7, 9),
(8, 11, 1),
(10, 11, 10),
(11, 12, 9),
(12, 16, 9);

-- --------------------------------------------------------

--
-- Table structure for table `notes`
--

CREATE TABLE `notes` (
  `id` int(5) NOT NULL,
  `name` varchar(500) NOT NULL,
  `tags` varchar(300) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL,
  `description` text NOT NULL,
  `createdAt` date NOT NULL,
  `updateAt` date NOT NULL,
  `owner` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Dumping data for table `notes`
--

INSERT INTO `notes` (`id`, `name`, `tags`, `description`, `createdAt`, `updateAt`, `owner`) VALUES
(1, 'Indomie', 'Indomie, Goreng, Aceh', 'Indomie adalah merek mi instan yang diproduksi oleh Indofood CBP, anak perusahaan Indofood Sukses Makmur di Indonesia. Indofood sendiri merupakan produsen mi instan terbesar di dunia, dengan 16 pabrik, 15 miliar paket Indomie diproduksi setiap tahun.', '2023-07-21', '2023-07-28', 1),
(7, 'How to print in c++', 'Tutorial, C++', '#include <iostream>\r\n\r\nint main() {\r\n	std::cout << \"Hello, world!\" << std::endl;\r\n\r\n	return 0;\r\n }', '2023-07-21', '2023-07-21', 1),
(11, 'Ford GT40 Mk II', 'Car, Sport, History', 'Ford GT40 adalah mobil balap ketahanan performa tinggi yang ditugaskan oleh Ford Motor Company. Mobil ini tumbuh dari proyek \"Ford GT\" (untuk Grand Touring), upaya untuk bersaing dalam balapan mobil sport jarak jauh Eropa, melawan Ferrari, yang memenangkan balapan bergengsi Le Mans 24 Jam dari tahun 1960 hingga 1965. Ford berhasil dengan GT40, memenangkan balapan tahun 1966 hingga 1969.', '2023-07-28', '2023-08-04', 9),
(12, 'HTML tag test', '', '<h1>Hello World</h1>\r\n\r\n<div>\r\n	<span>hai hello</span>\r\n	<ul>\r\n		<li>1</li>\r\n		<li>2</li>\r\n		<li>3</li>\r\naaaaaaaaaa\r\n	</ul>\r\n</div>', '2023-07-28', '2023-07-28', 1),
(16, 'hai', 'hai', 'hello world', '2023-08-04', '2023-08-04', 1);

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE `user` (
  `id` int(5) NOT NULL,
  `username` varchar(100) NOT NULL,
  `password` varchar(200) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`id`, `username`, `password`) VALUES
(1, 'edsel', '9180ff4e0d99acab0fbe11c1e9d9efa8'),
(9, 'hello', '5d41402abc4b2a76b9719d911017c592'),
(10, 'hai', '42810cb02db3bb2cbb428af0d8b0376e');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `collaboration`
--
ALTER TABLE `collaboration`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `notes`
--
ALTER TABLE `notes`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `username` (`username`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `collaboration`
--
ALTER TABLE `collaboration`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT for table `notes`
--
ALTER TABLE `notes`
  MODIFY `id` int(5) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;

--
-- AUTO_INCREMENT for table `user`
--
ALTER TABLE `user`
  MODIFY `id` int(5) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
