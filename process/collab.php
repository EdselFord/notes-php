<?php

include "../database/collab.php";
include "../database/user.php";

if (isset($_POST['add'])) {
    $user_id = User::getByName($_POST['user'])['id'];
    $note_id = $_POST['note'];


    if (!$user_id) {
        header("Location: ../notes.php?id=$note_id&err=nf");
        die();
    }

    if (Collab::checkAlready($note_id, $user_id)) {
        header("Location: ../notes.php?id=$note_id&err=al");
        die();
    }

    Collab::create($note_id, $user_id);


    header("Location: ../notes.php?id=$note_id");
} else if (isset($_POST['remove'])) {
    $user_id = User::getByName($_POST['user'])['id'];
    $note_id = $_POST['note'];

    $collab = Collab::getByNoteUser($note_id, $user_id);

    if ($collab) {
        Collab::remove($collab['id']);

        header("Location: ../notes.php?id=$note_id");
        die();
    }

    header("Location: ../notes.php?id=$note_id&err=nfr");
}
