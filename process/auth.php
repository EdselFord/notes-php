<?php
session_start();
include "../database/user.php";



if (isset($_POST['login'])) {


    $user = User::auth($_POST['username'], $_POST['password']);

    if ($user) {
        $_SESSION['user'] = $user['id'];
        header("Location: ../index.php");
    } else {
        header("Location: ../auth.php?error");
    }
} else if (isset($_POST['register'])) {
    User::create($_POST['username'], $_POST['password']);

    $user = User::auth($_POST['username'], $_POST['password']);


    $_SESSION['user'] = $user['id'];

    header("Location: ../index.php");
} else if (isset($_POST['logout'])) {
    session_unset();
    session_destroy();

    header("Location: ../index.php");

}
