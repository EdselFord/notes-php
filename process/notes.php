<?php
include "../database/notes.php";
session_start();


if (isset($_POST['create'])) {
    $id = $_SESSION['user'];
    Notes::create($_POST['name'], $id);

    $note_id = Notes::getByName($_POST['name'], $id)['id'];

    header("Location: ../notes.php?id=$note_id");
} else if (isset($_POST['save'])) {
    Notes::update($_POST['id'], $_POST['name'], $_POST['tags'], $_POST['description']);

    header("Location: ../index.php");
    
} else if (isset($_GET['del'])) {
    Notes::delete($_GET['id']);

    header("Location: ../index.php");
}