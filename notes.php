<?php
session_start();
include "database/notes.php";
include "database/user.php";
include "database/collab.php";

$notes = Notes::getById($_GET['id'], $_SESSION['user']);
$collab = Collab::getByNote($_GET['id']);

foreach ($collab as [$id, $note, $user]) {
    if ($user == $_SESSION['user']) {
        $notes = Notes::getForCollab($_GET['id']);
        break;
    }
}

if (empty($notes)) {
    header("Location: index.php");
}
?>


<div>
    <form action="process/notes.php" method="post">
        <input name="id" type="hidden" value="<?= $notes["id"] ?>">
        <input name="name" type="text" style="all: unset; font-size: xx-large; font-weight: bold;" autocomplete="off" placeholder="Note title" value="<?= $notes["name"] ?>" required>
        <br>
        <input name="tags" type="text" style="all: unset; font-size: small; margin: 0 10px 20px 0" autocomplete="off" placeholder="Tag 1, Tag 2, Tag 3" value="<?= $notes["tags"] ?>">
        <br>
        <textarea id="textbox" name="description" style="all: unset; width: calc(100vw - 120px); height: calc(100vh - 270px);"><?= $notes['description'] ?></textarea>
        <div style="position: fixed; bottom: 0; right: 0; margin: 20px;">
            <button onclick="event.preventDefault(); document.location.href='display.php?id=<?= $notes['id'] ?>'">back</button>
            <?php if ($_SESSION['user'] == $notes['owner']) : ?>
                <button onclick="event.preventDefault(); document.location.href='process/notes.php?id=<?= $notes['id'] ?>&del'">delete</button>
            <?php endif ?>
            <button name="save" type="submit">save</button>
        </div>
    </form>

    <?php if ($_SESSION['user'] == $notes['owner']) : ?>
        <form action="process/collab.php" method="post">
            <fieldset style="width: fit-content;">
                <h3 style="margin: 0; margin-top: 10px">Collaboration</h3>
                <?php if (isset($_GET['err'])) : ?>
                    <p style="color: red; margin: 0; margin-top: 10px">
                        <?php
                        switch ($_GET['err']) {
                            case 'nf':
                                echo "Username tidak ditemukan";
                                break;
                            case 'al':
                                echo "User sudah menjadi kolabolator";
                                break;
                            case 'nfr':
                                echo "User tidak dalam kolaborasi";
                                break;
                            default:
                                echo "What?";
                        }

                        ?>
                    </p>
                <?php endif ?>
                <label for="username">Username</label>
                <input name="user" id="username" type="text" required>
                <input name="note" type="hidden" value="<?= $notes["id"] ?>">

                <button type="submit" name="add">Add</button>
                <button type="submit" name="remove">Remove</button>

                <br><br>

                <div style="display: flex; flex-direction: row;">
                <?php foreach ($collab as [$id, $note, $user_id]) :
                    $user = User::getById($user_id);

                ?>
                    <div style="width: fit-content; height: min-content; padding: 2px; margin: 1px; border: 1px solid black;">
                        <p style="font-size: small; margin: 0;"><?= $user['username'] ?></p>
                    </div>
                <?php endforeach ?>
                </div>
        </form>
    <?php endif ?>
</div>

<script>
    document.getElementById('textbox').addEventListener('keydown', function(e) {
        if (e.key == 'Tab') {
            e.preventDefault();
            var start = this.selectionStart;
            var end = this.selectionEnd;

            // set textarea value to: text before caret + tab + text after caret
            this.value = this.value.substring(0, start) +
                "\t" + this.value.substring(end);

            // put caret at right position again
            this.selectionStart =
                this.selectionEnd = start + 1;
        }
    });
</script>