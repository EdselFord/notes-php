<?php

include "connection.php";


class Notes
{

    static function arrtonotes($i) {
        return [
            "id" => $i[0], 
            "name" => $i[1], 
            "tags" => $i[2], 
            "description" => $i[3], 
            "createdAt" => $i[4], 
            "updateAt" => $i[5], 
            "owner" => $i[6]
        ];
    }

    static function create(string $name, int $owner)
    {
        global $con;

        $createdAt = date('Y-m-d');
        $updateAt = $createdAt;

        $con->query("INSERT INTO notes VALUES (0, '$name', '', '', '$createdAt', '$updateAt', '$owner')");
    }


    static function read($user_id)
    {
        global $con;
        try {
            $res = $con->query("SELECT * FROM notes WHERE owner='$user_id'")->fetch_all();

            $res = array_map(function ($i) {
                return Notes::arrtonotes($i);
            }, $res);

            return $res;

        } catch (Exception $e) {
            return [];
        }
    }

    static function getById($id, $user)
    {
        global $con;
        try {
            return $con->query("SELECT * FROM notes WHERE id='$id' AND owner='$user'")->fetch_assoc();
        } catch (Exception $e) {
            return [];
        }
    }

    static function getByName($name, $user)
    {
        global $con;
        try {
            return $con->query("SELECT * FROM notes WHERE name='$name' AND owner='$user'")->fetch_assoc();
        } catch (Exception $e) {
            return [];
        }
    }

    static function getForCollab($id)
    {
        global $con;
        try {
            return $con->query("SELECT * FROM notes WHERE id='$id'")->fetch_assoc();
        } catch (Exception $e) {
            return [];
        }
    }

    static function update($id, $name, $tags, $description) {
        global $con;

        $updateAt = date('Y-m-d');
        
        $con->query("UPDATE notes SET name='$name', tags='$tags', description='$description', updateAt='$updateAt' WHERE id='$id'");
    }

    static function delete($id) {
        global $con;

        $con->query("DELETE FROM notes WHERE id='$id'");
    }

    
}
