<?php
include "connection.php";

class Collab {
    static function create($note_id, $user_id) {
        global $con;



        $con->query("INSERT INTO collaboration VALUES (0, '$note_id', '$user_id')");
    }

    static function getNotesFromCollab($user_id) {
        global $con;

        try {
            $res = Collab::getByUser($user_id);

            $arr = [];
            if ($res) {
                foreach($res as [$id, $note_id, $user]) {
                    array_push($arr, Notes::getForCollab($note_id));

                }
            }

            return $arr;
        } catch(Exception $e) {
            return [];
        }
    }

    static function getByUser($user_id) {
        global $con;
        try {
            return $con->query("SELECT * FROM collaboration WHERE collaborator_id='$user_id'")->fetch_all();    
        } catch(Exception $e) {
            return [];
        }
    }

    static function getByNote($note_id) {
        global $con;
        try {
            return $con->query("SELECT * FROM collaboration WHERE notes_id='$note_id'")->fetch_all();    
        } catch(Exception $e) {
            return [];
        }
    }

    static function getByNoteUser($note_id, $user_id) {
        global $con;
        try {
            return $con->query("SELECT * FROM collaboration WHERE notes_id='$note_id' AND collaborator_id='$user_id'")->fetch_assoc();

        } catch(Exception $e) {
            return false;
        }
    }

    static function checkAlready($note_id, $user_id) {
        global $con;
        try {
            $res = $con->query("SELECT * FROM collaboration WHERE notes_id='$note_id' AND collaborator_id='$user_id'")->fetch_all();

            if (empty($res)) return false;

            return true;
        } catch(Exception $e) {
            return false;
        }
    }

    static function remove($id) {
        global $con;

        $con->query("DELETE FROM collaboration WHERE id='$id'");
    }
}