<?php
session_start();
include "database/notes.php";
include "database/user.php";
include "database/collab.php";

$notes = Notes::getById($_GET['id'], $_SESSION['user']);

$collab = Collab::getByNote($_GET['id']);

foreach ($collab as [$id, $note, $user]) {
    if ($user == $_SESSION['user']) {
        $notes = Notes::getForCollab($_GET['id']);
        break;
    }
}


if (empty($notes)) {
    header("Location: index.php");
}
?>


<div>
    <h1 name="name" type="text" style=" font-size: xx-large; font-weight: bold;"><?= htmlspecialchars($notes["name"])  ?></h1>
    <p style="font-size: small">Created at <?= date("D F j Y", strtotime($notes["createdAt"])) ?></p>
    <p style="font-size: small">Owned by <?= User::getById($notes['owner'])['username'] ?></p>
    <div style="display: flex; flex-direction: row;">
        <?php if ($notes['tags'] != "") : foreach (explode(",", $notes['tags']) as $tag) : ?>
                <div style="width: fit-content; height: min-content; padding: 2px; margin: 1px; border: 1px solid black;">
                    <p style="font-size: xx-small; margin: 0;">#<?= $tag ?></p>
                </div>
        <?php endforeach;
        endif ?>
    </div>
    <br>
    <span name="description" style="white-space: pre-wrap;"><?= htmlspecialchars($notes['description'])  ?></span>
    <div style="position: fixed; bottom: 0; right: 0; margin: 20px;">
        <button onclick="event.preventDefault(); document.location.href='index.php'">back</button>
        <button onclick="event.preventDefault(); document.location.href='notes.php?id=<?= $notes['id'] ?>'">edit</button>
    </div>


</div>