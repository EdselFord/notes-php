<?php
session_start();
if (isset($_SESSION['user'])) {
    header("Location: index.php");
}
?>

<form action="process/auth.php" method="post">
    <h1>Login</h1>
    <div style="width: min-content;display: grid; grid-template-columns: repeat(2, 1fr); grid-gap: 5px; margin-bottom: 5px">
        <label for="username">Username</label>
        <input name="username" id="username" type="text" required autocomplete="off">

        <label for="password">Password</label>
        <input name="password" id="password" type="password" required autocomplete="off">

    </div>
    <?php
    if (isset($_GET['error'])) :
    ?>
        <p style="color: red;">Username / Password Salah</p>
    <?php
    endif;
    ?>

    <button type="submit" name="register">Register</button>
    <button type="submit" name="login">Login</button>
</form>